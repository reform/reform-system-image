#!/bin/sh

set -eu

if [ $# -ne 1 ]; then
	echo "usage: $1 reform-system.img" >&2
	exit 1
fi

IMAGE=$1

cleanup() {
	test -n "$KERNELTMP" && rm -f "$KERNELTMP"
	test -n "$INITRDTMP" && rm -f "$INITRDTMP"
	test -n "$BOOTEXTMP" && rm -f "$BOOTEXTMP"
}

KERNELTMP=$(mktemp)
INITRDTMP=$(mktemp)

# boot partition must be on the same filesystem as the full system image or
# else copy_file_range(2) will not work
BOOTEXTMP=$(mktemp --tmpdir="$(dirname "$IMAGE")")

trap cleanup EXIT INT TERM QUIT

# should be 12582912
BOOTSTART=$(/usr/sbin/parted --machine "$IMAGE" unit B print | grep '^1:' | cut -d: -f 2)
BOOTSTART=${BOOTSTART%B}
# should be 511705088
BOOTSIZE=$(/usr/sbin/parted --machine "$IMAGE" unit B print | grep '^1:' | cut -d: -f 4)
BOOTSIZE=${BOOTSIZE%B}

# even though /boot is 488 MB, this is fast, because it's sparse and only 70 MB
# have to be actually copied
python3 sparsedd.py "$IMAGE" "$BOOTEXTMP" "$BOOTSTART" 0 "$BOOTSIZE"
kernelname=$(/sbin/debugfs "$BOOTEXTMP" -R "ls -p" 2>/dev/null | cut -d / -f 6 | grep vmlinuz-)
/sbin/debugfs "$BOOTEXTMP" -R "cat $kernelname" > "$KERNELTMP"
initrdname=$(/sbin/debugfs "$BOOTEXTMP" -R "ls -p" 2>/dev/null | cut -d / -f 6 | grep initrd.img-)
/sbin/debugfs "$BOOTEXTMP" -R "cat $initrdname" > "$INITRDTMP"
rm "$BOOTEXTMP"
BOOTEXTMP=

# We create a temporary file using a file descriptor because the cleanup trap
# will not be called anymore after having replaced the shell process with qemu
# using exec.
KERNELFD=3
while test -h "/proc/self/fd/$KERNELFD"; do
        KERNELFD=$((KERNELFD + 1))
done
INITRDFD=$((KERNELFD + 1))
while test -h "/proc/self/fd/$INITRDFD"; do
        INITRDFD=$((INITRDFD + 1))
done
eval exec "$KERNELFD<"'"$KERNELTMP"'
eval exec "$INITRDFD<"'"$INITRDTMP"'
rm -f "$KERNELTMP" "$INITRDTMP"
KERNELTMP=
INITRDTMP=

set --
cpuname=$(lscpu | awk '/Model name:/ {print $3}' | tr '\n' '+')
ncpu=$(lscpu | awk '/Core\(s\) per socket:/ {print $4}' | tr '\n' '+')
if [ "$cpuname" = "Cortex-A53+Cortex-A73+" ] && [ "$ncpu" = "2+4+" ]; then
	# crude detection of the big.LITTLE heterogeneous setup of cores on the
	# amlogic a311d bananapi
	#
	# https://lists.nongnu.org/archive/html/qemu-devel/2020-10/msg08494.html
	# https://gitlab.com/qemu-project/qemu/-/issues/239
	# https://segments.zhan.science/posts/kvm_on_pinehone_pro/#trouble-with-heterogeneous-architecture
	set -- taskset --cpu-list 2,3,4,5
fi

# If the image filename contains a comma, then that comma must be escaped by
# prefixing it with another comma or otherwise output filenames are able to
# inject drive options to qemu (and load the wrong file).
IMAGE_ESCAPED="$(printf "%s" "$IMAGE" | sed 's/,/,,/g')"

# Replace the shell process with the qemu process.
# This allows running this script inside wrappers like timeout(1) which send
# signals to their child process.
exec "$@" qemu-system-aarch64 -netdev user,id=net0 -device virtio-net-pci,netdev=net0 \
	-device virtio-gpu-pci,edid=on,xres=1024,yres=768 \
	-vnc :94,share=force-shared \
	-device virtio-rng-pci,rng=rng0 -smp 4 -cpu max \
	-machine type=virt,gic-version=max,accel=kvm:tcg -no-user-config \
	-name debvm-run -m 1G -nographic \
	-drive "media=disk,format=raw,discard=unmap,file=$IMAGE_ESCAPED,if=virtio,cache=unsafe" \
	-object rng-random,filename=/dev/urandom,id=rng0 -device virtio-gpu-pci \
	-device virtio-keyboard-pci -device virtio-tablet-pci \
	-kernel "/proc/self/fd/$KERNELFD" -initrd "/proc/self/fd/$INITRDFD" -append 'root=LABEL=reformsdroot'
